package bigint

import (
	"errors"
	"regexp"
	"strconv"
)

type Bigint struct {
	Value string
}

var ErrorBadInput = errors.New("bad input, please input only number")

func checkInput(num string) (bool, error) {
	if match, err := regexp.MatchString(`^[+-]?[0-9]*$`, num); err != nil {
		return false, err
	} else {
		return match, nil
	}
}

// Return a new value
func NewInt(num string) (Bigint, error) {
	var f bool
	var err error
	if f, err = checkInput(num); err != nil {
		return Bigint{}, err
	}
	if !f {
		return Bigint{}, ErrorBadInput
	}

	return Bigint{Value: num}, nil
}

func (z *Bigint) Set(num string) error {
	var f bool
	var err error

	if f, err = checkInput(num); err != nil {
		return err
	}

	if !f {
		return ErrorBadInput
	}

	z.Value = num
	return nil
}

func adder(a, b Bigint) Bigint {
	ans := ""

	len_a := len(a.Value)
	len_b := len(b.Value)

	if len_a > len_b {
		l := len_a - len_b
		for i := 0; i < l; i++ {
			b.Value = "0" + b.Value
		}
	} else {
		l := len_b - len_a
		for i := 0; i < l; i++ {
			a.Value = "0" + a.Value
		}
	}

	q := 0
	for i := len(a.Value) - 1; i >= 0; i-- {
		a1, _ := strconv.Atoi(string(a.Value[i]))
		b1, _ := strconv.Atoi(string(b.Value[i]))

		res := a1 + b1 + q

		if res > 9 {
			q = 1
			res %= 10
		} else {
			q = 0
		}
		ans = strconv.Itoa(res) + ans
	}

	if q > 0 {
		ans = strconv.Itoa(q) + ans
	}

	return Bigint{Value: ans}
}

func Add(a, b Bigint) Bigint {

	if a.Value[0] != '-' {
		if b.Value[0] != '-' {
			return Bigint{Value: adder(a, b).Value}
		} else {
			if a.Value == b.Value[1:] {
				return Bigint{Value: "0"}
			} else if a.Value > b.Value[1:] {
				return Bigint{Value: subtract(a, b.Abs()).Value}
			} else {
				return Bigint{Value: "-" + subtract(a, b.Abs()).Value}
			}
		}
	} else {
		if b.Value[0] != '-' {
			if a.Value[1:] == b.Value {
				return Bigint{Value: "0"}
			} else if a.Value[1:] > b.Value {
				return Bigint{Value: "-" + subtract(a.Abs(), b).Value}
			} else {
				return Bigint{Value: subtract(a.Abs(), b).Value}
			}
		} else {
			return Bigint{Value: "-" + adder(a.Abs(), b.Abs()).Value}
		}
	}
}

func subtract(a, b Bigint) Bigint {
	ans := ""

	len_a := len(a.Value)
	len_b := len(b.Value)

	if len_a > len_b {
		l := len_a - len_b
		for i := 0; i < l; i++ {
			b.Value = "0" + b.Value
		}

		for i := len(a.Value) - 1; i >= 0; i-- {
			a1, _ := strconv.Atoi(string(a.Value[i]))
			b1, _ := strconv.Atoi(string(b.Value[i]))

			res := a1 - b1

			ans = strconv.Itoa(res) + ans
		}

	} else if len_a < len_b {
		l := len_b - len_a
		for i := 0; i < l; i++ {
			a.Value = "0" + a.Value
		}

		for i := len(a.Value) - 1; i >= 0; i-- {
			a1, _ := strconv.Atoi(string(a.Value[i]))
			b1, _ := strconv.Atoi(string(b.Value[i]))

			res := b1 - a1

			ans = strconv.Itoa(res) + ans
		}

	} else {
		if a.Value[0] > b.Value[0] {
			for i := len(a.Value) - 1; i >= 0; i-- {
				a1, _ := strconv.Atoi(string(a.Value[i]))
				b1, _ := strconv.Atoi(string(b.Value[i]))

				res := a1 - b1

				ans = strconv.Itoa(res) + ans
			}
		} else {
			for i := len(a.Value) - 1; i >= 0; i-- {
				a1, _ := strconv.Atoi(string(a.Value[i]))
				b1, _ := strconv.Atoi(string(b.Value[i]))

				res := b1 - a1

				ans = strconv.Itoa(res) + ans
			}
		}
	}

	return Bigint{Value: ans}
}

func Sub(a, b Bigint) Bigint {

	if a.Value == b.Value {
		return Bigint{Value: "0"}
	} else {
		if a.Value[0] == '-' {
			if b.Value[0] == '-' {
				if a.Value[1:] > b.Value[1:] {
					return Bigint{Value: "-" + subtract(a.Abs(), b.Abs()).Value}
				} else {
					return Bigint{Value: subtract(a.Abs(), b.Abs()).Value}
				}

			} else {
				return Bigint{Value: "-" + adder(a.Abs(), b).Value}
			}
		} else {
			if b.Value[0] == '-' {
				return Bigint{Value: adder(a, b.Abs()).Value}
			} else {
				if a.Value > b.Value {
					return Bigint{Value: subtract(a, b).Value}
				} else {
					return Bigint{Value: "-" + subtract(a, b).Value}
				}

			}
		}
	}

}

func Multiply(a, b Bigint) Bigint {
	//
	// MATH Multiply
	//
	return Bigint{Value: ""}
}

func Mod(a, b Bigint) Bigint {
	//
	// MATH Mod
	//
	return Bigint{Value: ""}
}

func (x *Bigint) Abs() Bigint {

	if x.Value[0] == '-' || x.Value[0] == '+' {
		return Bigint{Value: x.Value[1:]}
	}

	return Bigint{Value: x.Value}
}
