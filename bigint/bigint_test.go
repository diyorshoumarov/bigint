package bigint_test

import (
	"bootcamp/bigint/bigint"
	"testing"
)

func TestAbs(t *testing.T) {
	a := bigint.Bigint{
		Value: "-1",
	}

	b := a.Abs()
	if b.Value != "1" {
		t.Errorf("Abs FAILED: expected 1 but got %v", b)
	}

	a = bigint.Bigint{
		Value: "+1",
	}

	b = a.Abs()
	if b.Value != "1" {
		t.Errorf("Abs FAILED: expected 1 but got %v", b)
	}

	a = bigint.Bigint{
		Value: "1",
	}

	b = a.Abs()
	if b.Value != "1" {
		t.Errorf("Abs FAILED: expected 1 but got %v", b)
	}
}

func TestNewInt(t *testing.T) {
	str := "123123132"
	a, err := bigint.NewInt(str)
	if err != nil {
		t.Errorf("NewInt FAILED: %v", err)
	}
	if a.Value != str {
		t.Errorf("NewInt FAILED: expected %s but got %s", str, a.Value)
	}

	str2 := "123asd123"
	_, err2 := bigint.NewInt(str2)
	if err2 != bigint.ErrorBadInput {
		t.Errorf("NewInt FAILED: it should return %v", bigint.ErrorBadInput)
	}
}

func TestAdd(t *testing.T) {
	res := bigint.Add(
		bigint.Bigint{Value: "10"},
		bigint.Bigint{Value: "20"},
	)

	if res.Value != "30" {
		t.Errorf("Add FAILED: expected value 30 but got %v", res.Value)
	}

	res = bigint.Add(
		bigint.Bigint{Value: "-10"},
		bigint.Bigint{Value: "20"},
	)

	if res.Value != "10" {
		t.Errorf("Add FAILED: expected value 10 but got %v", res.Value)
	}

	res = bigint.Add(
		bigint.Bigint{Value: "10"},
		bigint.Bigint{Value: "-20"},
	)

	if res.Value != "-10" {
		t.Errorf("Add FAILED: expected value -10 but got %v", res.Value)
	}

	res = bigint.Add(
		bigint.Bigint{Value: "-10"},
		bigint.Bigint{Value: "-20"},
	)

	if res.Value != "-30" {
		t.Errorf("Add FAILED: expected value -30 but got %v", res.Value)
	}
}

func TestSub(t *testing.T) {
	res := bigint.Sub(
		bigint.Bigint{Value: "1"},
		bigint.Bigint{Value: "2"},
	)

	if res.Value != "-1" {
		t.Errorf("Add FAILED: expected value -1 but got %v", res.Value)
	}

	res = bigint.Sub(
		bigint.Bigint{Value: "-1"},
		bigint.Bigint{Value: "2"},
	)

	if res.Value != "-3" {
		t.Errorf("Add FAILED: expected value -3 but got %v", res.Value)
	}

	res = bigint.Sub(
		bigint.Bigint{Value: "1"},
		bigint.Bigint{Value: "-2"},
	)

	if res.Value != "3" {
		t.Errorf("Add FAILED: expected value 3 but got %v", res.Value)
	}

	res = bigint.Sub(
		bigint.Bigint{Value: "-1"},
		bigint.Bigint{Value: "-2"},
	)

	if res.Value != "1" {
		t.Errorf("Add FAILED: expected value 1 but got %v", res.Value)
	}
}
